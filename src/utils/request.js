import axios from "axios";

const http = axios.create({
  baseURL: "/api",
  timeout: 10000, //10秒钟后终止请求。
});

//添加请求拦截器，
http.interceptors.request.use(
  function (config) {
    //请求发送之前处理
    return config; //请求完成之后返回完整的请求数据。
  },
  function (error) {
    //请求错误时处理程序。
    return Promise.reject(error); //返回错误对象。
  }
);

//添加响应拦截器，用于处理响应消息
http.interceptors.response.use(
  function (response) {
    //响应成功之后处理程序。
    return response; //返回响应数据。
  },
  function (error) {
    //响应错误时处理程序。
    return promise.reject(error); //返回错误对象。
  }
);

export default http;
