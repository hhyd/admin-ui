import http from "@/utils/request";

//请求首页数据
export const getData = () => {
    //返回一个Promise对象，用于等待请求结果
     return http.get("/home/getData"); 
    }

export const getUser = (params)=>{
    // 返回用户列表
    return http.get('/user/getUser',params)
}

// post 和 get 不同，参数是data对象，可以参考：
// https://www.axios-http.cn/docs/api_intro
export const addUser = (data)=>{
    return http.post('/user/add',data)
}

export const editUser = (data)=>{
    return http.post('/user/edit',data)
}

export const delUser = (data)=>{
    return http.post('/user/del',data)
}

export const getMenu = (data)=>{
    return http.post('/permission/getMenu',data)
}
