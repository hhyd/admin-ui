import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import router from './router';
import store from './store';
import './api/mock'
import Cookie from 'js-cookie'

// 添加全局前置路由守卫
router.beforeEach((to, from, next) => {
  // 判断token是否存在
  const token = Cookie.get('token');
  // token 不存在，说明当前用户未登录，应跳转至登录
  if (!token && to.name !== 'login') {
      next({name: 'login'})
  } else if (token && to.name === 'login') {// 如果token存在，说明登录成功，跳转首页
      next({name: 'home'})
  } else {
      next()
  }
})

Vue.config.productionTip = false
Vue.use(ElementUI);


new Vue({
  router,
  store,
  render: h => h(App),
  created() {
      store.commit('addMenu',router)
  }
}).$mount('#app')
