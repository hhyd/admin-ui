import Cookie from "js-cookie";
export default {
  state: {
    isCollapse: false, //用于菜单展开、收起
    tabList: [
      {
        path: "/",
        name: "home",
        label: "首页",
        icon: "s-home",
        url: "Home/home",
      },
    ], //面包屑数据
    menu: [],
  },
  mutations: {
    collapseMenu(state) {
      state.isCollapse = !state.isCollapse;
    },
    // 更新面包屑数据
    selectMenu(state, val) {
      // 判断添加数据是否为首页
      if (val.name !== "home") {
        // 判断如果不存在
        const index = state.tabList.findIndex((item) => item.name === val.name);
        // 如果不存在
        if (index === -1) {
          state.tabList.push(val);
        }
      }
    },
    // 删除指定的 tag 数据，第一个参数是固定的
    closeTag(state, item) {
      const index = state.tabList.findIndex((val) => val.name === item.name);
      state.tabList.splice(index, 1);
    },
    // 设置菜单
    setMenu(state, val) {
      state.menu = val;
      Cookie.set("menu", JSON.stringify(val));
    },
    // 动态注册路由
    // 动态注册路由
    addMenu(state, router) {
      // 判断当前缓存中是否有数据
      if (!Cookie.get("menu")) return;
      const menu = JSON.parse(Cookie.get("menu"));
      state.menu = menu;
      // 动态组装路由数据
      const menuArray = [];
      menu.forEach((item) => {
        // 有子菜单
        if (item.children) {
          item.children = item.children.map((item) => {
            item.component = () => import(`../views/${item.url}`);
            return item;
          });
          menuArray.push(...item.children);
        } else {
          // 没有子菜单
          item.component = () => import(`../views/${item.url}`);
          menuArray.push(item);
        }
      });
      // console.log('menuArray',menuArray);
      //路由的动态添加
      menuArray.forEach((item) => {
        router.addRoute("Main", item);
      });
    },

  }//end mutations
};
