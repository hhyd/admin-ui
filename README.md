 **通用管理后台 Admin-ui** 



   https://cn.vuejs.org/guide/introduction.html

### 1、淘宝 npm 域名即将停止解析，npmmirror 镜像站大重构升级

- http://npm.taobao.org 和 http://registry.npm.taobao.org 将在 2022.06.30 号正式下线和停止 DNS 解析。

```
npm config set registry https://registry.npmmirror.com/
npm config get registry
```

### 2、安装CLI

```
https://cli.vuejs.org/zh/ 
npm install -g @vue/cli
# OR
yarn global add @vue/cli
```

### 3、创建一个项目

```
vue create my-project
# OR
vue ui
```

我的项目如下：

```
vue create admin-ui
cd admin-ui
yarn serve
```

### 4、安装elementUI

网址：https://element.eleme.cn/#/zh-CN
`npm i element-ui -S`
在 main.js 中写入以下内容：


```
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(ElementUI);
```

### 5、安装 babel-plugin-component，按需引入

```
npm install babel-plugin-component -D
   babel.config.js修改如下：
module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset',
    ["@babel/preset-env", { "modules": false }]
  ],
  "plugins": [
    [
      "component",
      {
        "libraryName": "element-ui",
        "styleLibraryName": "theme-chalk"
      }
    ]
  ]
}
```

### 6、安装Vue Router

 网址： https://v3.router.vuejs.org/zh/
Vue Router 是 Vue.js 的官方路由。它与 Vue.js 核心深度集成，让用 Vue.js 构建单页应用变得轻而易举。功能包括：

- 嵌套路由映射
- 动态路由选择
- 模块化、基于组件的路由配置
- 路由参数、查询、通配符
- 展示由 Vue.js 的过渡系统提供的过渡效果
- 细致的导航控制
- 自动激活 CSS 类的链接
- HTML5 history 模式或 hash 模式
- 可定制的滚动行为
- URL 的正确编码

 npm install vue-router@3.6.5 //vue2使用3.X版本，此处必须要指定版本，否则将下载最新版


- 注意：vue2使用3.X版本，此处必须要指定版本，否则将下载最新版
- 可以到 https://www.npmjs.com/ 查询3.X的最新版本


#### 6.1新增router/index.js

```
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import User from '../views/User.vue'
import Main from '../views/Main.vue'



Vue.use(VueRouter)

// 1. 定义 (路由) 组件（XXX.vue）
// 2. 定义路由
const routes =[
    {
      path: '/',
      component: Main,
      children: [
        //嵌套子路由
        { path: '/home', component: Home },
        { path: '/user', component: User }
      ]
    }
  ]

  // 3. 创建 router 实例，然后传 `routes` 配置
const router = new VueRouter({
    routes // (缩写) 相当于 routes: routes
  })
  //4、对外暴露
  export default router
```


#### 6.2 在main.js中创建和挂载根实例

```
 import router from './router'; //引入router实例
 
 new Vue({
  router, //  创建和挂载根实例，router 配置参数注入路由，从而让整个应用都有路由功能
  render: h => h(App),
}).$mount('#app')

```

#### 6.3在APP.vue中渲染路由组件

```
  <!-- 路由出口 -->
  <!-- 路由匹配到的组件将渲染在这里 -->
  <router-view></router-view>
```



#### 6.4嵌套路由需要在相应的主页面渲染路由组件，如Main.vue

```
<template>
  <!-- 注意：在vue2的组件中，每一个组件<template>下只有能一个根节点，插入第二个元素就会报错 -->
  <div>
    <el-container>
      <el-aside width="200px">Aside</el-aside>
      <el-container>
        <el-header>Header</el-header>
        <el-main>
          <!-- 路由出口 -->
          <!-- 路由匹配到的组件将渲染在这里 -->
          <router-view></router-view>
        </el-main>
      </el-container>
    </el-container>
  </div>
</template>
  
<script>
export default {};
</script>
  
<style></style>
```

### 7、安装less

less官方文档 https://less.bootcss.com/

Less （Leaner Style Sheets 的缩写） 是一门向后兼容的 CSS 扩展语言。这里呈现的是 Less 的官方文档（中文版），包含了 Less 语言以及利用 JavaScript 开发的用于将 Less 样式转换成 CSS 样式的 Less.js 工具。


```
npm install less@4.1.2
npm install less-loader@6.0.0 
```



```
<style lang="less" scoped>
.el-menu-vertical-demo:not(.el-menu--collapse) {
    width: auto;
    min-height: 400px;
}
.el-menu{
    height: 100vh;
    h3{
        color: #fff;
        font-size: 16px;
        text-align: center;
        line-height: 48px;
    }
}
</style>
```

### 8.安装vuex

- https://v3.vuex.vuejs.org/zh/
- Vuex 是一个专为 Vue.js 应用程序开发的状态管理模式。它采用集中式存储管理应用的所有组件的状态，并以相应的规则保证状态以一种可预测的方式发生变化。

```npm i vuex@3.6.2 ```
